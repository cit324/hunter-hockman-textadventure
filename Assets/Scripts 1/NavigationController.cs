﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationController : MonoBehaviour
{
    public Room currentRoom;
    public List<Room> allRooms;
    //string is direction ("north") and the room is the room that is in that direction
    private Dictionary<string, Room> exitRooms = new Dictionary<string, Room>();

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(Unpack());
    }

    public string Unpack()
    {
        string description = currentRoom.description; 
        foreach(Exit exit in currentRoom.exits)
        {
            description += " " + exit.description;
            exitRooms.Add(exit.theDirection.ToString(), exit.room);
            
        }
        return description;
    }

    public bool SwitchRoom(string direction)
    {
        
        if (exitRooms.ContainsKey(direction)) //if exit in that direction exists
        {
            //GameManager.instance.inventory.Contains("key")
            //makes sure the orb and key are used to unlock the correct doors
            if ((false == IsLocked(direction)) || (IsLocked(direction) && GameManager.instance.inventory.Contains("key")) 
                || (IsLocked(direction) && GameManager.instance.inventory.Contains("orb") && currentRoom.name == "Smokey")) //room is not locked
            {
                currentRoom = exitRooms[direction];
                exitRooms.Clear(); //resets the exits
                Debug.Log("room switched");
                return true;
            }
            else
            {
                Debug.Log("exit is locked");
                return false;
            }
        }
        return false;
    }

    public int GetNumExits()
    {
        return currentRoom.exits.Length;
    }

    public bool IsLocked(string direction)
    {
        foreach (Exit exit in currentRoom.exits)
        {
            if (direction == exit.theDirection.ToString())
                return exit.locked;
        } 
        return false; //assume not locked
    }

    public bool TakeItem(string item) //returns true if the item is in the room
    {
        foreach(string i in currentRoom.items)
        {
            if (i == item)
                return true;
        }
        return false;
    }

    public Room GetRoomByName(string name)
    {
        foreach(Room r in allRooms)
        {
            if(r.name == name) //found a match
            {
                return r;
            }
        }
        return currentRoom; //return default if not found -- start room
    }
}
