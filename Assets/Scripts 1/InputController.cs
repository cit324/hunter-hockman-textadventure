﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
    public InputField inputField;
    public Toggle mode;
    public Text displayText; //reference to large text area
    public Dictionary<string, string> valid = new Dictionary<string, string>(); //our valid commands
    public Image background;
    public Text toggleLabel;
    public Text sliderLabel;
    public Text placeHolder;
    public Text inputFieldText;
    public Slider sizeSlider;//font size slider

    private string story;
    private bool darkMode; //if true - darkmode is on; if false darkmode is off

    public delegate void GameOver(); //set up the delegate and template
    public event GameOver onGameOver; //create event linked to delegate
 
    void Start()
    {
        //UpdateDisplayText("You wake up in a dark room."); //added
        inputField.onEndEdit.AddListener(GetInput);
        mode.onValueChanged.AddListener(ModeChange);
        sizeSlider.onValueChanged.AddListener(ChangeFontSize);
        //adds commands and command descriptions
        valid.Add("go" , "go: Allows you to move to another room if an exit exists");  //valid commands
        valid.Add("get" , "get: Adds an item to inventory if an item is in the room");
        valid.Add("restart", "restart: Restarts the game from the very beginning");
        valid.Add("save", "save: Saves game");
        valid.Add("commands", "commands: Displays a list of valid commands");
        valid.Add("inventory", "inventory: Displays current inventory");

        //load saved preferences correctly
        if (PlayerPrefs.HasKey("DarkMode"))
        {
            int isDark = PlayerPrefs.GetInt("DarkMode");
            darkMode = isDark == 1;
            SetTheme();
        }
        else //key not found
        {
            PlayerPrefs.SetInt("DarkMode", 1); //by default the dark mode is enabled
            PlayerPrefs.Save();
        }
        
        //sets font size to player preference
        if (PlayerPrefs.HasKey("FontSize"))
        {
            int fSize = PlayerPrefs.GetInt("FontSize");
            sizeSlider.value = fSize;
            ChangeFontSize(fSize);
        }
        else //key not found
        {
            PlayerPrefs.SetInt("FontSize", 24); //by default the font size is 24
            PlayerPrefs.Save();
        }
    }

    //notifies of change in preference
    void ModeChange(bool userInput)
    {
        //player pref
        darkMode = userInput;
        PlayerPrefs.SetInt("DarkMode", darkMode ? 1 : 0);
        PlayerPrefs.Save();
        SetTheme();
    }
    
    void ChangeFontSize(float newSize)//changes font size to the value of newSize
    {
        int newFontSize = (int)newSize;
        placeHolder.fontSize = newFontSize;
        displayText.fontSize = newFontSize;
        PlayerPrefs.SetInt("FontSize", newFontSize);
        PlayerPrefs.Save();
    }

    void SetTheme()
    {
        if (darkMode)
        {
            background.color = Color.black;
            displayText.color = Color.white;
            toggleLabel.color = Color.white;
            sliderLabel.color = Color.white;
            placeHolder.color = Color.white;
            inputFieldText.color = Color.white;
            mode.isOn = true;
        }
        else
        {
            background.color = Color.white;
            displayText.color = Color.black;
            toggleLabel.color = Color.black;
            sliderLabel.color = Color.black;
            placeHolder.color = Color.black;
            inputFieldText.color = Color.black;
            mode.isOn = false;
        }
    }

    void GetInput(string userInput)
    {

        inputField.text = "";  // clear user input
        inputField.ActivateInputField(); // put user back in input field (so they don't have to click!)

        if(userInput != "") //not empty
        {
            char[] splitInfo = { ' ' }; //delimeter
            string[] inputCommands = userInput.Trim().ToLower().Split(splitInfo); //break user input up
            if (valid.ContainsKey(inputCommands[0]))
            { //is this a valid command
                Debug.Log(userInput + " found");
                UpdateDisplayText(userInput);
                if (inputCommands[0] == "go")
                {
                    bool result = GameManager.instance.nav.SwitchRoom(inputCommands[1]);

                    if (result) //if true - switched to a valid room
                    {
                        UpdateDisplayText(GameManager.instance.nav.Unpack());
                        if (GameManager.instance.nav.GetNumExits() == 0)
                        {
                            UpdateDisplayText("Type 'Restart' to Restart");
                            if(onGameOver != null) //are any functions connected -- is anyone listening?
                                onGameOver();
                        }
                    }
                    else
                        UpdateDisplayText("Sorry, there is no exit in that direction or the door is locked");
                }
                else if (inputCommands[0] == "restart") //lowercase!!!
                    SceneManager.LoadScene(0);
                else if(inputCommands[0] == "get")
                {
                    // get the key and add it to inventory
                    if (GameManager.instance.nav.TakeItem(inputCommands[1]))
                    {
                        //add to inventory
                        GameManager.instance.inventory.Add(inputCommands[1]);
                        UpdateDisplayText(inputCommands[1] + " item has been added to the inventory");
                    }
                    else
                        UpdateDisplayText("That item is not in this room. Please try again");
                }
                else if(inputCommands[0] == "save")
                {
                    //call to save function
                    GameManager.instance.Save();
                }
                else if (inputCommands[0] == "commands")
                {
                    //prints all commands in the valid dictionary 
                    UpdateDisplayText("Commands list:"); 
                    UpdateDisplayText(valid["go"]);
                    UpdateDisplayText(valid["get"]);
                    UpdateDisplayText(valid["restart"]);
                    UpdateDisplayText(valid["save"]);
                    UpdateDisplayText(valid["commands"]);
                    UpdateDisplayText(valid["inventory"]);

                }
                else if (inputCommands[0] == "inventory")
                {
                    //prints everything in the inventory if there is anything in the inventory
                    if (GameManager.instance.inventory.Count < 1)
                    {
                        UpdateDisplayText("No items in inventory");
                    }
                    else 
                    { 
                        UpdateDisplayText("Current Inventory:");
                        for (int i = 0; i < GameManager.instance.inventory.Count; i++)
                        {
                            UpdateDisplayText(GameManager.instance.inventory[i]);
                        }
                    }
                }
                //UpdateDisplayText(userInput); //moved this up
            }
            else
            {
                Debug.Log(userInput + " not found");
                UpdateDisplayText("Command not found");
            }
        }
        //Debug.Log(userInput);
    } //end of GetInput function

    public void UpdateDisplayText(string msg)
    {
        story += "\n" + msg;
        displayText.text = story;
    }
}
