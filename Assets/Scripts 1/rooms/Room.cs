﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/Room")]
public class Room : ScriptableObject
{
    public string roomName;
    [TextArea]
    public string description;
    //exits
    public Exit[] exits; //added Friday
    public string[] items;
}
